from google.appengine.ext import ndb


class User(ndb.Model):
    p_created = ndb.DateTimeProperty(auto_now_add=True, name='created')
    p_updated = ndb.DateTimeProperty(auto_now=True, name='updated')

    # first name, last name, middle name
    p_name = ndb.StringProperty(name='name')

    # properties
    @property
    def id_str(self):
        return str(self.key.id())

    @property
    def name(self):
        return self.p_name

    @name.setter
    def name(self, value):
        self.p_name = value.strip()

    def to_object(self):
        data = {}
        data['id'] = self.key.id()
        data['id_str'] = self.id_str
        data['name'] = self.name

        return data
