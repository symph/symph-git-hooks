from google.appengine.ext import ndb


class Diff(ndb.Model):
    p_created = ndb.DateTimeProperty(auto_now_add=True, name='created')
    p_updated = ndb.DateTimeProperty(auto_now=True, name='updated')

    p_content = ndb.TextProperty()

    # properties
    @property
    def id_str(self):
        return str(self.key.id())

    @property
    def content(self):
        return self.p_content

    @content.setter
    def content(self, value):
        self.p_content = value.strip()

    def to_object(self):
        data = {}
        data['id'] = self.key.id()
        data['id_str'] = self.id_str
        data['content'] = self.content

        return data
