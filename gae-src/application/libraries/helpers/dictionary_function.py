import logging
import json


def wrap_json(user_response, response, callback=False):
    """Creates a wrapped json response for the controller.

    Note:
        This function will right away write to the stdout response.

    Args:
        user_response (HttpResponse): The reponse object for the user.

        response (dict): The dictionary containing the data to be written to
            the response stdout.

        callback (def): a callback function, but currently not used.

    Examples:
        >>> wrap_json(response, self.tv)
        # will wrap the data inside the self.tv dictionary
    """
    if "code" in response['api_response']:
        try:
            response_code = int(response['api_response']['code'])
            user_response.set_status(
                response_code, "Error")
        except:
            logging.exception("Cannot set Status Code Header")
    user_response.headers['Content-Type'] = "application/json"
    response_text = json.dumps(response['api_response'], False, False)
    user_response.out.write(response_text)
    logging.debug("RESPONSE:")
    logging.debug(response_text)
    return