DOMAIN_ROUTE = r'<:localhost|symph-git-hooks\.appspot\.com|.*-dot-symph-git-hooks\.appspot\.com|.*\.symph-git-hooks\.appspot\.com|symph-git-hooks-staging\.appspot\.com|.*-dot-symph-git-hooks-staging\.appspot\.com|.*\.symph-git-hooks-staging\.appspot\.com>'

"""  USER STATUS """
USER_STATUS_ACTIVE = 'ACTIVE'
USER_STATUS_INACTIVE = 'INACTIVE'

""" ALERT MESSAGE TYPES """
ALERT_SUCCESS = 'ALERT_SUCCESS'
ALERT_ERROR = 'ALERT_ERROR'
ALERT_INFO = 'ALERT_INFO'

"""  ROUTES """
ROUTE_DIFF_HTML = "DIFF_HTML"
