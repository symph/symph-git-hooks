from application.handlers.base import BaseHandler
import json
from google.appengine.api import urlfetch
from application.config.credentials import SLACK_TOKEN


class SendApiHandler(BaseHandler):
    def post(self):
        result = urlfetch.fetch(url="https://symph.slack.com/services/hooks/incoming-webhook?token={}".format(SLACK_TOKEN),
                                payload=self.request.body,
                                method=urlfetch.POST,
                                headers={"Content-type": "application/json"})
        self.api_response.update({
            "description": result.content,
            "code": 200,
            "success": True
        })
        self.api_render()
