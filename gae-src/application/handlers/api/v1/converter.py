import logging
import pdfcrowd
import sys
from application.handlers.base import BaseHandler
import urllib
import StringIO
from PIL import Image
from google.appengine.api import urlfetch
from poster.encode import multipart_encode, MultipartParam
from application.config.credentials import SLACK_LEGACY_TOKEN
from application.config.credentials import PDFCROWD_API_KEY
from application.config.credentials import PDFCROWD_CLIENT_NAME
from google.appengine.ext import deferred
from google.appengine.api.taskqueue import TaskRetryOptions
from application.models.diff import Diff


class DiffApiHandler(BaseHandler):
    def get(self):
        self.api_render()

    def post(self):

        channels = self.request.get("channels")
        file_content = self.request.get('file')
        username = self.request.get('username')

        diff = Diff()
        # remove first and last string
        diff.content = file_content.strip()[1:-1]
        diff_key = diff.put()

        if diff_key:
            options = TaskRetryOptions(task_retry_limit=0)
            deferred.defer(
                self.send_file_to_slack,
                channels,
                file_content,
                diff_key.id(),
                username,
                _retry_options=options
            )

            self.api_response.update({
                "description": "Processing text to image",
                "code": 200,
                "success": True
            })
        else:
            self.api_response.update({
                "description": "Error",
                "code": 500,
                "success": False
            })

        self.api_render()

    @classmethod
    def convert_html_to_image(cls, html="", diff_id=""):
        output_string = ""
        try:
            # create the API client instance
            client = pdfcrowd.HtmlToImageClient(PDFCROWD_CLIENT_NAME, PDFCROWD_API_KEY)

            # configure the conversion
            client.setOutputFormat('png')
            image_data = client.convertUrl('https://symph-git-hooks.appspot.com/diff-html/{}'.format(str(diff_id)))
            # image_data = client.convertString(html)
            output_string = StringIO.StringIO(image_data)

        except pdfcrowd.Error as why:
            # report the error to the standard error stream
            sys.stderr.write('Pdfcrowd Error: {}\n'.format(why))

        return output_string

    @classmethod
    def send_file_to_slack(cls, channels, file, diff_id, username):
        logging.debug(diff_id)
        logging.debug(username)
        file_name = "{} - {}.png".format(username, str(diff_id))
        file_type = "application/octet-stream"

        file_content = cls.convert_html_to_image(
            file, diff_id
        )

        payload = {
            'file': MultipartParam('file', filename=file_name, filetype=file_type,
                                   fileobj=file_content),
            'channels': channels,
            'token': SLACK_LEGACY_TOKEN,
            'content': 'https://symph-git-hooks.appspot.com/diff-html/{}'.format(str(diff_id))
        }
        to_post = multipart_encode(payload)

        urlfetch.fetch(
            url="https://slack.com/api/files.upload",
            payload="".join(to_post[0]),
            method=urlfetch.POST,
            headers=to_post[1]
        )
