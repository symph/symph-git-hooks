from application.handlers.base import BaseHandler
import json
import datetime
import logging
from google.appengine.api import urlfetch
from application.config.credentials import KEYRR_SLACK_TOKEN
from application.config.credentials import CLINT_SLACK_TOKEN
from application.config.credentials import BOBBY_SLACK_TOKEN


class AttendanceApiHandler(BaseHandler):
    def get(self):
        if not self.check_leave() and datetime.datetime.now().strftime("%A") not in ["Saturday", "Sunday"]:
            user_slack_tokens = {
                "keyrr": KEYRR_SLACK_TOKEN
            }

            result = urlfetch.fetch(url="https://slack.com/api/chat.postMessage?token={}&channel={}&text={}".format(
                    user_slack_tokens.get(self.request.get("user", "keyrr"), "keyrr"), 'symph-attendance', 'present'
                ),
                method=urlfetch.POST,
                deadline=60
            )

            self.api_response.update({
                "description": result.content,
                "code": 200,
                "success": True
            })

        else:
            self.api_response.update({
                "description": "No transaction for this day.",
                "code": 404,
                "success": False
            })

        self.api_render()

    def post(self):
        self.api_render()

    def check_leave(self):
        result = urlfetch.fetch(url="https://symph-monitor.appspot.com/api/v1/attendance?date-from={}&date-to={}&user-id=All&status=All".format(
                datetime.datetime.now().strftime("%Y-%m-%d"), datetime.datetime.now().strftime("%Y-%m-%d")
            ),
            method=urlfetch.GET,
            deadline=60
        )

        response = json.loads(result.content)

        logging.debug(response)

        is_leave = False
        is_wfh = False
        is_sick = False

        for leave_user in response.get("attendance", [{}])[0].get("user_leave", []):
            if "keyrr@sym.ph" in leave_user.get("email", ""):
                is_leave = True

        for leave_user in response.get("attendance", [{}])[0].get("user_sick", []):
            if "keyrr@sym.ph" in leave_user.get("email", ""):
                is_sick = True
        
        for wfh_user in response.get("attendance", [{}])[0].get("user_work_at_home", []):
            if "keyrr@sym.ph" in wfh_user.get("email", ""):
                is_wfh = True

        return is_leave or is_sick or is_wfh
