from application.libraries.helpers.dictionary_function import wrap_json
import logging
import os
## webapp exceptions handlers

import sys
import re
class ErrorHandler:

    def init(self, request):
        self.rx = r'^(/api/v(\d{1,2})(/{0,1}\w+){0,})'
        """this regex will ensure to output json only to url's forming /api/v*/*"""

        self.tv = {}
        self.local = False
        if "127.0.0.1" in request.uri or "localhost" in request.uri:
            self.local = True
        self.tv["local"] = self.local

        self.tv["version"] = os.environ['CURRENT_VERSION_ID']
        if "?" in request.uri:
            self.tv["current_base_url"] = (
                request.uri[0:(request.uri.find('?'))]
            )
        else:
            self.tv["current_base_url"] = request.uri
        return self

    @classmethod
    def not_implemented(self, request, res, exception):
        self = self().init(request)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logging.error(
            str(exc_value),
            exc_info=(exc_type, exc_value, exc_traceback))

        if re.search(self.rx, str(request.path)):
            self.tv["api_response"] = {
                "code": 501,
                "success": False,
                "description": (
                    'The ' +
                    request.method +
                    ' method is not supported for this endpoint.'
                )
            }
            res.headers["Content-Type"] = "application/json"
            wrap_json(res, self.tv)
        else:
            res.write("UnsupportedMethodError")

    @classmethod
    def log_exception(self, request, res, exception):
        self = self().init(request)
        exc_type, exc_value, exc_traceback = sys.exc_info()

        if re.search(self.rx, str(request.path)):
            self.tv["api_response"] = {
                "code": 500,
                "success": False,
                "description": re.sub(
                    self.rx, "", str(exc_value))
            }

            if hasattr(exception, "msg"):
                self.tv["api_response"]["error_msg"] = exception.msg

            if hasattr(exception, "path"):
                self.tv["api_response"]["path"] = \
                    [str(p) for p in exception.path]

            if hasattr(exception, "errors"):
                self.tv["api_response"]["errors"] = [
                    {
                        "msg": error.msg,
                        "path": [str(p) for p in error.path]
                    }
                    for error in exception.errors
                ]

            if hasattr(exc_type, "code"):
                logging.info("Error code: " + str(exc_type.code))
                logging.info(
                    str(exc_value),
                    exc_info=(exc_type, exc_value, exc_traceback))
                self.tv["api_response"]["code"] = int(exc_type.code)

            else:
                logging.error(
                    str(exc_value),
                    exc_info=(exc_type, exc_value, exc_traceback))

            if '(key=Key(\'' in self.tv['api_response']['description']:
                self.tv['api_response']['description'] = 'BadValueError'
            res.headers["Content-Type"] = "application/json"
            wrap_json(res, self.tv)
        else:
            logging.error(
                str(exc_value),
                exc_info=(exc_type, exc_value, exc_traceback))
            res.write("ServerError ** edit me!!")
    
    @classmethod
    def log_not_found(self, request, res, exception):
        self = self().init(request)
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logging.error(
            str(exc_value),
            exc_info=(exc_type, exc_value, exc_traceback))

        if re.search(self.rx, str(request.path)):
            self.tv["api_response"] = {
                "code": exc_type.code or 404,
                "success": False,
                "description": "This endpoint does not exist."
            }

            res.headers["Content-Type"] = "application/json"
            wrap_json(res, self.tv)
        else:
            res.write("EndpointNotFound")
