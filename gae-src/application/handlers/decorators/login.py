from application.config.constants import ROUTE_LOGIN


def required(fn):
    '''So we can decorate any RequestHandler with #@login.required'''
    def wrapper(self, *args, **kwargs):
        if not self.user:
            self.redirect(self.uri_for(ROUTE_LOGIN))
            return

        return fn(self, *args, **kwargs)
    return wrapper


def admin_required(fn):
    '''So we can decorate any RequestHandler with @login.admin_required'''
    def wrapper(self, *args, **kwargs):
        if not self.user:
            self.redirect(
                self.uri_for(ROUTE_LOGIN, next=self.request.path)
            )
            return

        if self.user.admin:
            return fn(self, *args, **kwargs)
        else:
            self.redirect(
                self.uri_for(
                    ROUTE_LOGIN,
                    error="You are not authorized to access the page."
                )
            )
    return wrapper
