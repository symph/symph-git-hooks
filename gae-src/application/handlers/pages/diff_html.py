from application.handlers.base import BaseHandler
from application.models.diff import Diff


class DiffHtmlHandler(BaseHandler):
    def get(self, diff_id=None):
    	diff = Diff.get_by_id(long(diff_id))
    	if not diff:
    		self.error(500)

    	else:
    		self.tv["diff_content"] = diff.content
        	self.render("templates/diff.html")