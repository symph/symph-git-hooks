import webapp2
from webapp2_extras import routes

from application.handlers.error import ErrorHandler

from application.config.constants import DOMAIN_ROUTE
from application.config.constants import ROUTE_DIFF_HTML

# PAGE HANDLERS
from application.handlers.pages.diff_html import DiffHtmlHandler

# API V1 HANDLERS
from application.handlers.api.v1.converter import DiffApiHandler
from application.handlers.api.v1.send_slack import SendApiHandler
from application.handlers.api.v1.attendance import AttendanceApiHandler

# PAGE ROUTES
app = webapp2.WSGIApplication([
    routes.DomainRoute(
        DOMAIN_ROUTE, [
            webapp2.Route(
                r'/diff-html/<:.*>',
                handler=DiffHtmlHandler,
                name=ROUTE_DIFF_HTML
            )
        ]
    )
], debug=True)

# API ROUTES
api = webapp2.WSGIApplication([
    routes.DomainRoute(
        DOMAIN_ROUTE, [
            webapp2.Route(
                '/api/v1/diff',
                handler=DiffApiHandler
            ),
            webapp2.Route(
                '/api/v1/send_message',
                handler=SendApiHandler
            ),
            webapp2.Route(
                '/api/v1/attendance',
                handler=AttendanceApiHandler
            ),
            webapp2.Route(
                '/api/v1/attendance/contacts',
                handler=AttendanceApiHandler
            ),
            webapp2.Route(
                '/api/v1/attendance/messages',
                handler=AttendanceApiHandler
            ),
        ])
], debug=True)

api.error_handlers[500] = ErrorHandler.log_exception
api.error_handlers[501] = ErrorHandler.not_implemented
api.error_handlers[405] = ErrorHandler.not_implemented
api.error_handlers[404] = ErrorHandler.log_not_found
